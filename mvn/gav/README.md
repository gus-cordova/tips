# Extracting a Java project's GAV parameters from the command line

To extract parameters from a java project's `pom.xml` file in a form that's usable from the shell, the most effective and efficient way I've found is using the `exec:exec` goal:

```bash
# From the directory where pom.xml exists
$ mvn -q \
	-Dexec.executable=echo \
	-Dexec.args='VERSION=${project.version}' \
	--non-recursive \
	exec:exec
VERSION=1.2.3-SNAPSHOT
$ _
```

It sends its output to STDOUT, and because it has the `-q` flag you don't need to filter out any additional noise caused by mvn's verbose diagnostics.

A simple example to figure out the destination jar filename could be:

```bash
$ get() {
	mvn -q  -Dexec.executable=echo -Dexec.args="$@" --non-recursive exec:exec
}
$ echo "Filename: $(get '${project.artifactId}-${project.version}.${project.packaging}')"
Filename: projectName-1.2.3-SNAPSHOT.jar
```

