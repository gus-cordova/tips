# Tips, Hints, Shortcuts, and so on.

This is an aggregate of all sorts of tips and shortcuts and tricks and whatnots that I've been gathering. Some are for coding, others for system managing, others are "how-to" guides to solve a specific problem, and so forth.

Here is the index of all this stuff, each link takes you to the `README` for that specific subject.

Enjoy!

-gca

## Index

- [Extracting Java project's GAV parameters from the command line](./mvn/gav/)
- [Automatically creating a Gitlab merge request](./sh/auto-merge-req/)



