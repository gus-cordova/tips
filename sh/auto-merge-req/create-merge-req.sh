#!/bin/bash
#
# Expect these env vars:
#	$CI_PROJECT_URL
#	$CI_PROJECT_ID
#	$CI_COMMIT_REF_NAME
#	$CI_PIPELINE_IID
#	$GITLAB_USER_ID
#	$PRIVATE_TOKEN
#
# Also depends on 'jq' and 'curl' being available.
#
set -euo pipefail
[[ ${DEBUG} ]] && set -x

# Extract the host where the server is running, and add the URL to the APIs
[[ ${CI_PROJECT_URL} =~ ^https?://[^/]+ ]] \
	&& API="${BASH_REMATCH[0]}/api/v4/projects/${CI_PROJECT_ID}"

api() { curl -sL -H "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "$@" ; }

# Figure out the default branch
TARGET_BRANCH=$(api "$API" | jq -r .default_branch)

# Create the merge request
api -X POST \
    -H "Content-Type: application/json" \
	  -d "{\"id\":${CI_PROJECT_ID},\"source_branch\":\"${CI_COMMIT_REF_NAME}\",\"target_branch\":\"${TARGET_BRANCH}\",\"remove_source_branch\":true,\"title\":\"WIP: Post-release merge (pipeline ${CI_PIPELINE_IID})\"}" \
    "$API/merge_requests"
# Fin.
