# Automatically Create Gitlab Merge Request

Sometimes its desirable to automatically create a Merge Request in a Gitlab pipeline upon reaching a certain point, this script helps you with that:

```bash
#!/bin/bash
#
# Expect these env vars:
#	$CI_PROJECT_URL
#	$CI_PROJECT_ID
#	$CI_COMMIT_REF_NAME
#	$CI_PIPELINE_IID
#	$GITLAB_USER_ID
#	$PRIVATE_TOKEN
#
# Also depends on 'jq' and 'curl' being available.
#
set -euo pipefail
[[ ${DEBUG} ]] && set -x

# Extract the host where the server is running, and add the URL to the APIs
[[ ${CI_PROJECT_URL} =~ ^https?://[^/]+ ]] \
	&& API="${BASH_REMATCH[0]}/api/v4/projects/${CI_PROJECT_ID}"

# Figure out the default branch
TARGET_BRANCH=$(
	curl -s -H "PRIVATE-TOKEN:${PRIVATE_TOKEN}" "${API}" | jq -r .default_branch)

# Parameters for the merge request
BODY="{
    \"id\": ${CI_PROJECT_ID},
    \"source_branch\": \"${CI_COMMIT_REF_NAME}\",
    \"target_branch\": \"${TARGET_BRANCH}\",
    \"remove_source_branch\": true,
    \"title\": \"WIP: Post-release merge (pipeline ${CI_PIPELINE_IID})\"
}";

# Create the merge request
curl -X POST "${API}/merge_requests" \
	-H "PRIVATE-TOKEN:${PRIVATE_TOKEN}" \
	-H "Content-Type: application/json" \
	--data "${BODY}";
```

The `PRIVATE_TOKEN` variable needs to be configured from a user that has write permission in the repository, that is the only custom variable that is not provided by Gitlab automation directly.

An example usage in a Gitlab pipeline is this:

```yaml
stages:
	- openMR
	- otherStages
	
tag-prod-release:
	- stage: openMR
	before_script: []  # nothing to do before
	variables:
		PRIVATE_TOKEN: ${THE_PRIVATE_TOKEN}
	script:
		- /path/to/script/create-merge-req.sh
```

